# Sequences

These are notes from reading  “Fluent Python” by Luciano Ramalho

* Flat sequences faster to use, but limited to atomic data
* Container sequences more flexible, holding references rather than values, but can be surprising when holding mutable objects

### List comprehensions 
* Can be also called ‘listcomps’
* Only use listcomps if you are doing something with the produced list
* Try to keep short
* Listcomps have local scope
* You can nest loops like so, where the outermost loop is written first
```python
[(x, y) for x in xs for y in ys]
```

### Generator expressions
> A genexp saves memory because it yields items one by one using the iterator protocol instead of building a whole list just to feed another constructor.   
>   
> Extract from “Fluent Python” by Luciano Ramalho  
* Can be also called  ‘genexps’
* The same syntax as listcomps but enclose with parentheses (unless only argument in a function call)
```python
> xs = [1, 2, 3, 4]
> tuple(x for x in xs)
=> (1, 2, 3, 4)
```
* Can save memory instead of building up a list just to feed a loop

### Tuples
* Can be created with `a, b`
* All list methods that do not involved adding or removing items (mutable methods) are supported by tuples with one exception `__reversed__` (`reversed(tuple)` works)
* Tuples can be used as unnamed records instead of just immutable lists
* Can be deconstructed `a, b, *rest = (1, 2, 3, 4)` (this applies to all iterables)
* For loops can retrieve tuple items separately `for a, b in [(1, 2), (3,4)]`
* Can format strings like `’%s/%s’ % ('a', 'b')`, each item will be assigned separately
* Can argument splat `m(*(1, 2))` is the same as `m(1, 2)`
* Examples of `*`
```python
> a, b, *rest = range(5)
> a, b, rest
=> (0, 1, [2, 3, 4])
> a, *body, c, d = range(5) 
> a, body, c, d
=> (0, [1, 2], 3, 4)
```
* Unpack a nested tuple by deliminating the variables of the nest with parentheses

#### Named tuples
* See ‘Data model’
* `NamedTuple._fields` returns tuple of field names
* `named_tuple._asdict()` turns into an ordered dict

### Slicing
* `s[a:b:c]` steps items by `c`
* You can slice in reverse by making `c` negative
* `[a:b:c]` is equivalent to `slice(a, b, c)`
* Instead of hardcoding slices you can named them
```python
SLICE = slice(0, 5)
list[SLICE]
```

#### Multi-dimensional slicing and ellipsis
* Multi-dimensional slicing `list[a:b, c:d]` exists, and can be used for user defined types and extensions. Built in Python sequences are one dimensional so can support only one slice.
* There also exists usage of `...` which aliases to an `ellipsis` class and used as a multi-dimensional shortcut (hahahahahaha). The book has a link to [Tentative NumPy Tutorial](http://wiki.scipy.org/Tentative_NumPy_Tutorial) as more explanation.

#### Assigning to slices
* Mutable sequences can be modified in place by assigning `slice` to a list
```python
> l = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] 
> l[2:5] = [20, 30]
=> [0, 1, 20, 30, 5, 6, 7, 8, 9]
> del l[5:7]
=> [0, 1, 20, 30, 5, 8, 9]
```

#### Using `+` and `*` with sequences
* `+` and `*` create new sequences
* When multiplying sequences containing mutable items, be careful  - there will be multiple references to one object. The best way of initialising lists of lists is using list comps

#### Augmented assignments
* `+=` calls `__iadd__`, falling back to `__add__` and producing a new object
* `*=` calls `__imul__`, falling back to `__mul__` and producing a new object
* Repeated concatenation of immutable objects is inefficient (barring `str`)
> - Putting mutable items in tuples is not a good idea.   
> - Augmented assignment is not an atomic operation — we just saw it throwing an exception after doing part of its job.   
> - Inspecting Python bytecode is not too difficult, and is often helpful to see what is going on under the hood.   
>   
> Excerpt from “Fluent Python” by Luciano Ramalho  

#### Sorting
* A sort is stable if it preserves the relative ordering of items that compare equal

#### Managing ordered sequences with `bisect`
* Use `bisect.bisect` to quickly find the index of the point where an item can be inserted into an ordered sequence. When faced with equality it returns the index to the right, `bisect_left` returns leftmost index of equal objects
* `bisect.bisect` * can be a faster replacement for the index method when searching through long ordered sequences of numbers.
* `bisect.insort` inserts an item into a sequence while keeping sequence in ascending order

### Arrays
* Arrays are more efficient than lists for sequences of numbers
* On creation you provide a typecode, which determines the C type used to store items in array. Python won’t let you insert any items that don’t match the array type
* Reading and writing binary files with `array` is a lot faster than doing the equivalent with text files, and the file sizes are smaller

### Memory views
> The built-in memoryview class is a shared-memory sequence type that lets you handle slices of arrays without copying bytes.  
>   
> Excerpt from “Fluent Python” by Luciano Ramalho  
* Needs more looking into and learning about

### NumPy and SciPy
* Provide easy multi-dimensional array creation and manipulation, along with other fast and efficient number crunching operations

### Deques and queues
* There are a few queue types - not all covered here

#### Deques
* A double ended “queue”, for fast operations on both ends of a collection
* Can be bounded so when you add items from one end, it pushes items off from the other end
* Optimised for end operations, not as fast for operations in middle of collection
* `append` and `popleft` are atomic so safe in multi-threaded applications

#### Queues
* You can use queues for safe communication between threads
* Instead of pushing items off when bounded, they wait to add the items until when a space has opened up
