# Dictionaries and sets

Notes from reading “Fluent Python” by Luciano Ramalho

* Hash tables are what are used to implement dicts and sets
* Using `isinstance(dict, abc.Mapping)` over checking type so alternative mapping types can be used
* An object is hashable if it has a `__hash__` and `__eq__` method, with equal objects having the same hash value
* Dicts must have hashable keys

### Dictcomps
* Use same syntax as listcomps or genexps, returning `key: value`
```python
> l = [('a', 1), ('b', 2)]
> {key: value + 1 for key, value in l if value > 1}
=> {'b': 3}
```

### Methods
* Dict ‘views’ are like ‘windows’ to the underlying dict - for example in `a = d.items()`, `a` will change if `d` changes
[python - What are dictionary view objects? - Stack Overflow](https://stackoverflow.com/questions/8957750/what-are-dictionary-view-objects)

#### `setdefault()`
* When finding and updating a value, `d.setdefault(v, default)` can often be a good choice, and save dict lookups
```python
index.setdefault(word, []).append(location)
# Copied from Example 3-4 of "Fluent Python" by Luciano Ramalho
```

### Flexible key lookups
#### `defaultdict`
* `d = defaultdict(func)` will set `d[k] = func()` when `d[k]` is called but doesn’t exist
* The passed in function is called a `callable` and is set as the attribute `default_factory`
* A default dict will only assign defaults on `__getitem__`

#### `__missing__`
* `__missing__` is called by `__getitem__` if a key is not found

### Dict types
* `OrderedDict` maintains keys in insertion order
* `ChainMap` holds a list of mappings which can be searched as one
* `Counter` holds an integer count for each key, updating key increments counter
* `UserDict` a Python implementation like standard `dict`, designed to be subclassed

#### Subclassing `UserDict`
* It is more preferable to subclass from `UserDict` than `dict` - there is more in depth explanation of this later - maybe because `dict` has C implementations so more methods have to be overwritten
* `UserDict` subclasses `MutableMapping` so has a couple of useful methods

### Immutable mappings
* There is a `MappingProxyType` you can wrap dicts with, that provides a window into the dict but prevents changes
```python
d = {1: 'a'} # d is changeable
d_proxy = MappingProxyType(d) # d_proxy isn't
```

### Sets
* There is `set` and `frozenset`
* Sets are a collection of unique objects
* Initialise `set` with `set()`, build as `{1, 2}`
* Turning a list into a set removes duplication
* Set elements must be hashable
* They can use set operations `&` (intersection), `|` (union) and `-` (difference)
* The infix operators take sets, but the underlying methods can take other collections as arguments

#### Setcomps
* Same syntax as dictcomps but return a single element
* Will automatically ignore repeated elements

### Hash tables
* A ‘sparse’ array, an array which always has empty cells (also called buckets). Python tries to keep at least 1/3rd of the buckets empty
* For `dict` hash table, the populated buckets have references to key and reference to value
* To put an item in a hash table, calculate the hash value  of item key (with `hash()` built-in or `__hash__`)
* Hash values for similar (not equal) objects should be very different

#### Hash table algorithm
![](images/dicts_and_sets_0.png?raw=true) 
From “Fluent Python” by Luciano Ramalho
* [java - Hashing algorithm and HashMap - Stack Overflow](https://stackoverflow.com/questions/18992826/hashing-algorithm-and-hashmap)
* [Hash function - Wikipedia](https://en.wikipedia.org/wiki/Hash_function)

#### How `dicts` work because of hash tables
* Keys must be hashable objects
* `dicts` take up a lot of memory because hash tables are sparse
* Key search is very fast
* Key ordering depends on insertion order because if there is a collision, the object will be put somewhere else
* Key ordering may change on object insertion if Python decides the hash table needs to grow to accommodate new key

#### How sets work because of hash tables
* All of the things that apply to `dict` also apply to sets
* The hash table buckets contain only a reference to the elements
