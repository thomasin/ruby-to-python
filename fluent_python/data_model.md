# Data model

These are notes from reading  “Fluent Python” by Luciano Ramalho

* Python utilises special methods (surrounded by underscores) to implement language features
  * `collection[key]` is called by Python as `collection.__get__item(key)`
  * `len(collection)` is called as `collection.__len__()`
  * `ins1 + ins2` is called as `ins1.__add__(ins2)`
  * `__repr__ ` is used to get string representations of an instance for inspection
* These special methods can be referred to as ‘dunder methods’
* Advantages to using dunder methods are
> 1. The users of your classes don’t have to memorise arbitrary method names for standard operations (“How to get the number of items? Is it .size() .length() or what?”)  
> 2. It’s easier to benefit from the rich Python standard library and avoid reinventing the wheel, like the random.choice function.   
>   
> Excerpt from “Fluent Python” by Luciano Ramalho  
* These special methods should not be invoked directly, just implemented in user classes if need be
* [3. Data model — Python 3.7.3rc1 documentation](https://docs.python.org/3/reference/datamodel.html) lists special methods

#### Notes
* `collections.namedtuple` can be used similar to Ruby `OpenStruct`
```python
Struct = collections.namedtuple('Struct', ['attr1, attr2'])
Struct(attr1='a', attr2='b')
```
