These notes are from (skim) reading [Python Crash Course](https://www.oreilly.com/library/view/python-crash-course/9781457197185/)  
Python version is 3.5 > (a few 3.6 > things like importing from folders with no `__init__.py`)

### Basics
#### Numbers
* Dividing two integers `4 / 3` produces floats rather than integers

#### Booleans
* Python is `True` and `False` rather than `true` and `false`

#### Built-in
* Python has a lot of random built in functions like `sorted`, `str`, `len`

#### Functions/methods
* Methods are functions defined within a class (and callable via `class.method()`)
* Parentheses are required rather than optional `’print(‘hello’)` rather than `puts ‘hello’`
* Parentheses are also required when defining function `def m():` rather than `def m`
* Python relies on indentation to define blocks rather than `end` statements
* Blocks are also defined with a colon `:` after the definition (eg `if True:`
* Keyword args can be used in the place of positional args rather than defined separately
```ruby
> def method(a, b:); end
> method('val1', b: 'val2')
```
```python
> def method(a, b):
> method(a='val1', b='val')
> method('val1', 'val2')
```
* For required keyword arguments in Python
```python
> def method(*, a, b)
> method(a='val1', b='val2')
```
* Python has explicit rather than implicit returns, need to put `return` even at end of functions
* `def m(**info):` turns unspecified keyword args into a hash

#### Lists
* Instead of map `[f(x) for x in xs]`
* Instead of filter `[x for x in xs if x % 2]`
* Can use `xs[:3]` to slice up to (not including) the 3rd index from the beginning
* Or `xs[3:]` to slice from (including) the third index to the end
* `xs[:]` copies the entire list
* Python has a keyword `in` for checking list membership `if ‘a’ in list:` or `not in`
* Empty lists are evaluated as `False`

#### Tuples
* Python has tuples! `(1, 2, 3)`
* You can access values but not assign them using `(1, 2)[i]`
* Can loop tuples with for loops

> “When compared with lists, tuples are simple data structures. Use them when you want to store a set of values that should not be changed throughout the life of a program.”  
>   
> Excerpt From: Eric Matthes. “Python Crash Course.” Apple Books.   

#### If statements
* As per 'Functions/methods', if blocks are defined with `semicolons` and indentation (no `end` keyword)
```python
if True:
  print('true')
else:
  print('false')
```
* Use `and` and `or` rather than `&&` and `||`
* `elif` rather than `elsif`

#### Dictionaries
* Hashes are called dictionaries
* Dictionaries are unordered
* Written as `{ 'a': 1, 'b': 2 }` rather than `{ ‘a’ => 1, ‘b’ => 2 }`
* Looping through dictionaries `for key, value in dict.items()`

#### Modules
[The Definitive Guide to Python import Statements | Chris Yeh](https://chrisyeh96.github.io/2017/08/08/definitive-guide-python-imports.html)
* Import modules into files with `import x` where `x.py` is in the same directory. This `import` covers both Ruby keywords `require` and `include`
* When importing from subdirectories can use dot syntax `import x.y`
* `from x import y`

#### Classes
* A class is defined `class Beans():` rather than `class Beans`
* Initialise method `def __init__(self, a, b):` rather than `def initialize(a, b)` 
* Python methods need `self` as the first parameter `def m(self)` instead of magic `self`
* Instance variables set with `self.var = var` rather than `@var = var`
* Make an instance with `Beans(‘a’, ‘b’)` rather than `Beans.new(‘a’, ‘b’)`
* Private instance variables and methods are indicated with leading underscores instead of a `private` keyword `def __method():` and `self.__var = var` rather than `private def method` and `private attr_accessor :var` (if wanted without an `@` within the class)
* Define inheritance with `class Beans(Food):` rather than `class Beans < Food`
* When using `super` you have to explicitly call the method and pass args `super().method()` rather than them automatically being passed in

#### Files
* Interesting built in keywords for Python file reading and writing - this closes the file once it ends
```python
with open('file.txt') as file:
```
